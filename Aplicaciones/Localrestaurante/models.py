from django.db import models

# Create your models here.
class Provincia(models.Model):
    codigo_pro_al=models.AutoField(primary_key=True)
    nombre_pro_al=models.CharField(max_length=50)
    capital_pro_al=models.CharField(max_length=50)
    poblacion_pro_al=models.CharField(max_length=50)
    pais_pro_al=models.CharField(max_length=50)
    fotografia=models.FileField(upload_to='provincias', null=True,blank=True)


class Tipo(models.Model):
    codigo_tip_al=models.AutoField(primary_key=True)
    nombre_tip_al=models.CharField(max_length=50)
    descripcion_tip_al=models.CharField(max_length=50)
    categoria_tip_al=models.CharField(max_length=50)

class Ingrediente(models.Model):
    codigo_ing_al=models.AutoField(primary_key=True)
    nombre_ing_al=models.CharField(max_length=50)
    categoria_ing_al=models.CharField(max_length=50)
    cantidad_ing_al=models.CharField(max_length=50)
    fecha_caducidad_ing_al=models.DateField()
    imagen=models.FileField(upload_to='ingredientes', null=True, blank=True)

class Cliente(models.Model):
    codigo_cli_al=models.AutoField (primary_key=True)
    cedula_cli_al=models.CharField(max_length=10)
    apellido_cli_al=models.CharField(max_length=150)
    nombre_cli_al=models.CharField(max_length=150)
    direccion_cli_al=models.TextField()
    provincia=models.ForeignKey(Provincia,null=True,blank=True,on_delete=models.CASCADE)

class Pedido(models.Model):
    codigo_ped_al=models.AutoField (primary_key=True)
    nombre_ped_al=models.CharField (max_length=150)
    fecha_pedido_ped_al=models.DateField()
    direccion_ped_al=models.TextField()
    modo_pago_ped_al=models.CharField(max_length=150)
    cliente=models.ForeignKey(Cliente,null=True,blank=True,on_delete=models.CASCADE)

class Platillo(models.Model):
    codigo_pla_al=models.AutoField (primary_key=True)
    nombre_pla_al=models.CharField (max_length=150)
    descripcion_pla_al=models.CharField (max_length=150)
    precio_pla_al=models.CharField (max_length=50)
    disponibilidad_pla_al=models.CharField (max_length=150)
    tipo=models.ForeignKey(Tipo,null=True,blank=True,on_delete=models.CASCADE)

class Detalle(models.Model):
    codigo_det_al=models.AutoField (primary_key=True)
    fecha_detalle_det_al=models.DateField()
    cantidad_det_al=models.CharField (max_length=150)
    descripcion_det_al=models.CharField (max_length=150)
    total_det_al=models.CharField (max_length=150)
    pedido=models.ForeignKey(Pedido,null=True,blank=True,on_delete=models.CASCADE)
    platillo=models.ForeignKey(Platillo,null=True,blank=True,on_delete=models.CASCADE)

class Receta(models.Model):
    codigo_rec_al=models.AutoField (primary_key=True)
    nombre_rec_al=models.CharField (max_length=150)
    descripcion_rec_al=models.CharField (max_length=150)
    tipo_preparacion_rec_al=models.CharField (max_length=150)
    pasos_rec_al=models.CharField (max_length=150)
    platillo=models.ForeignKey(Platillo,null=True,blank=True,on_delete=models.CASCADE)
    ingrediente=models.ForeignKey(Ingrediente,null=True,blank=True,on_delete=models.CASCADE)
