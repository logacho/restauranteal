from django.shortcuts import render, redirect
from .models import *
from django.contrib import messages

# Create your views here.
def listadoProvincias(request):
    provinciasBdd=Provincia.objects.all()
    return render(request,'provincia.html',{'provincias':provinciasBdd})

def guardarProvincia(request):
    nombre_pro_al=request.POST["nombre_pro_al"]
    capital_pro_al=request.POST["capital_pro_al"]
    poblacion_pro_al=request.POST["poblacion_pro_al"]
    pais_pro_al=request.POST["pais_pro_al"]
    fotografia=request.FILES.get("fotografia")

    nuevoProvincia=Provincia.objects.create(nombre_pro_al=nombre_pro_al, capital_pro_al=capital_pro_al,
     poblacion_pro_al=poblacion_pro_al, pais_pro_al=pais_pro_al, fotografia=fotografia)

    messages.success(request, 'Provincia Guardado exitosamente')
    return redirect('/')

def eliminarProvincia(request,codigo_pro_al):
    provinciaEliminar=Provincia.objects.get(codigo_pro_al=codigo_pro_al)
    provinciaEliminar.delete()
    messages.success(request, 'Provincia Eliminado exitosamente')
    return redirect('/')

def editarProvincia(request,codigo_pro_al):
    provinciaEditar=Provincia.objects.get(codigo_pro_al=codigo_pro_al)
    return render(request,
    'editarProvincia.html',{'provincia':provinciaEditar})

def procesarActualizacionProvincia(request):
    codigo_pro_al=request.POST["codigo_pro_al"]
    nombre_pro_al=request.POST["nombre_pro_al"]
    capital_pro_al=request.POST["capital_pro_al"]
    poblacion_pro_al=request.POST["poblacion_pro_al"]
    pais_pro_al=request.POST["pais_pro_al"]
    #Insertando datos mediante el ORM de DJANGO
    provinciaEditar=Provincia.objects.get(codigo_pro_al=codigo_pro_al)
    provinciaEditar.nombre_pro_al=nombre_pro_al
    provinciaEditar.capital_pro_al=capital_pro_al
    provinciaEditar.poblacion_pro_al=poblacion_pro_al
    provinciaEditar.pais_pro_al=pais_pro_al

    provinciaEditar.save()
    messages.success(request,
      'Provincia ACTUALIZADO Exitosamente')
    return redirect('/')

def listadoTipos(request):
    tiposBdd=Tipo.objects.all()
    return render(request,'tipo.html',{'tipos':tiposBdd, 'navbar': 'tipo'})

def guardarTipo(request):
    nombre_tip_al=request.POST["nombre_tip_al"]
    descripcion_tip_al=request.POST["descripcion_tip_al"]
    categoria_tip_al=request.POST["categoria_tip_al"]


    nuevoTipo=Tipo.objects.create(nombre_tip_al=nombre_tip_al, descripcion_tip_al=descripcion_tip_al, categoria_tip_al=categoria_tip_al)

    messages.success(request, 'Tipo Guardado exitosamente')
    return redirect('/tipo')

def eliminarTipo(request,codigo_tip_al):
    tipoEliminar=Tipo.objects.get(codigo_tip_al=codigo_tip_al)
    tipoEliminar.delete()
    messages.success(request, 'Tipo Eliminado exitosamente')
    return redirect('/tipo')

def editarTipo(request,codigo_tip_al):
    tipoEditar=Tipo.objects.get(codigo_tip_al=codigo_tip_al)
    return render(request,
    'editarTipo.html',{'tipo':tipoEditar})

def procesarActualizacionTipo(request):
    codigo_tip_al=request.POST["codigo_tip_al"]
    nombre_tip_al=request.POST["nombre_tip_al"]
    descripcion_tip_al=request.POST["descripcion_tip_al"]
    categoria_tip_al=request.POST["categoria_tip_al"]
    #Insertando datos mediante el ORM de DJANGO
    tipoEditar=Tipo.objects.get(codigo_tip_al=codigo_tip_al)
    tipoEditar.nombre_tip_al=nombre_tip_al
    tipoEditar.descripcion_tip_al=descripcion_tip_al
    tipoEditar.categoria_tip_al=categoria_tip_al

    tipoEditar.save()
    messages.success(request,
      'Tipo ACTUALIZADO Exitosamente')
    return redirect('/tipo')

def listadoIngredientes(request):
    ingredientesBdd=Ingrediente.objects.all()
    return render(request,'ingrediente.html',{'ingredientes':ingredientesBdd, 'navbar': 'ingrediente'})

def guardarIngrediente(request):
    nombre_ing_al=request.POST["nombre_ing_al"]
    categoria_ing_al=request.POST["categoria_ing_al"]
    cantidad_ing_al=request.POST["cantidad_ing_al"]
    fecha_caducidad_ing_al=request.POST["fecha_caducidad_ing_al"]
    imagen=request.FILES.get("imagen")

    nuevoIngrediente=Ingrediente.objects.create(nombre_ing_al=nombre_ing_al, categoria_ing_al=categoria_ing_al, cantidad_ing_al=cantidad_ing_al,
     fecha_caducidad_ing_al=fecha_caducidad_ing_al, imagen=imagen)

    messages.success(request, 'Ingrediente Guardado exitosamente')
    return redirect('/ingrediente')

def eliminarIngrediente(request,codigo_ing_al):
    ingredienteEliminar=Ingrediente.objects.get(codigo_ing_al=codigo_ing_al)
    ingredienteEliminar.delete()
    messages.success(request, 'Ingrediente Eliminado exitosamente')
    return redirect('/ingrediente')

def editarIngrediente(request,codigo_ing_al):
    ingredienteEditar=Ingrediente.objects.get(codigo_ing_al=codigo_ing_al)
    return render(request,
    'editarIngrediente.html',{'ingrediente':ingredienteEditar})

def procesarActualizacionIngrediente(request):
    codigo_ing_al=request.POST["codigo_ing_al"]
    nombre_ing_al=request.POST["nombre_ing_al"]
    categoria_ing_al=request.POST["categoria_ing_al"]
    cantidad_ing_al=request.POST["cantidad_ing_al"]
    fecha_caducidad_ing_al=request.POST["fecha_caducidad_ing_al"]
    if 'imagen' in request.FILES:
            imagen = request.FILES.get("imagen")
    else:
            # Si no se proporciona una nueva imagen, conserva la existente
            in_existente = Ingrediente.objects.get(codigo_ing_al=codigo_ing_al)
            imagen = in_existente.imagen
    #Insertando datos mediante el ORM de DJANGO
    ingredienteEditar=Ingrediente.objects.get(codigo_ing_al=codigo_ing_al)
    ingredienteEditar.nombre_ing_al=nombre_ing_al
    ingredienteEditar.categoria_ing_al=categoria_ing_al
    ingredienteEditar.cantidad_ing_al=cantidad_ing_al
    ingredienteEditar.fecha_caducidad_ing_al=fecha_caducidad_ing_al
    ingredienteEditar.imagen=imagen
    ingredienteEditar.save()
    messages.success(request,
      'Ingrediente ACTUALIZADO Exitosamente')
    return redirect('/ingrediente')

def listadoClientes(request):
    clientesBdd=Cliente.objects.all()
    provinciasBdd=Provincia.objects.all()
    return render(request,'cliente.html',{'clientes':clientesBdd, 'provincias':provinciasBdd, 'navbar': 'cliente'})

def guardarCliente(request):
    id_pro=request.POST["id_pro"]
    provinciaSeleccionado=Provincia.objects.get(codigo_pro_al=id_pro)
    cedula_cli_al=request.POST["cedula_cli_al"]
    apellido_cli_al=request.POST["apellido_cli_al"]
    nombre_cli_al=request.POST["nombre_cli_al"]
    direccion_cli_al=request.POST["direccion_cli_al"]


    nuevoCliente=Cliente.objects.create(cedula_cli_al=cedula_cli_al, apellido_cli_al=apellido_cli_al, nombre_cli_al=nombre_cli_al,
    direccion_cli_al=direccion_cli_al, provincia=provinciaSeleccionado)

    messages.success(request, 'Cliente Guardado exitosamente')
    return redirect('/cliente')

def eliminarCliente(request,codigo_cli_al):
    clienteEliminar=Cliente.objects.get(codigo_cli_al=codigo_cli_al)
    clienteEliminar.delete()
    messages.success(request, 'Cliente Eliminado exitosamente')
    return redirect('/cliente')

def editarCliente(request,codigo_cli_al):
    clienteEditar=Cliente.objects.get(codigo_cli_al=codigo_cli_al)
    provinciasBdd=Provincia.objects.all()
    return render(request,
    'editarCliente.html',{'cliente':clienteEditar, 'provincias':provinciasBdd})

def procesarActualizacionCliente(request):
    id_pro=request.POST["id_pro"]
    provinciaSeleccionado=Provincia.objects.get(codigo_pro_al=id_pro)
    codigo_cli_al=request.POST["codigo_cli_al"]
    cedula_cli_al=request.POST["cedula_cli_al"]
    apellido_cli_al=request.POST["apellido_cli_al"]
    nombre_cli_al=request.POST["nombre_cli_al"]
    direccion_cli_al=request.POST["direccion_cli_al"]

    #Insertando datos mediante el ORM de DJANGO
    clienteEditar=Cliente.objects.get(codigo_cli_al=codigo_cli_al)
    clienteEditar.provincia=provinciaSeleccionado
    clienteEditar.cedula_cli_al=cedula_cli_al
    clienteEditar.apellido_cli_al=apellido_cli_al
    clienteEditar.nombre_cli_al=nombre_cli_al
    clienteEditar.direccion_cli_al=direccion_cli_al

    clienteEditar.save()
    messages.success(request,
      'Cliente ACTUALIZADO Exitosamente')
    return redirect('/cliente')

def listadoPedidos(request):
    pedidosBdd=Pedido.objects.all()
    clientesBdd=Cliente.objects.all()
    return render(request,'pedido.html',{'pedidos':pedidosBdd, 'clientes':clientesBdd, 'navbar': 'pedido'})

def guardarPedido(request):
    id_cli=request.POST["id_cli"]
    clienteSeleccionado=Cliente.objects.get(codigo_cli_al=id_cli)
    nombre_ped_al=request.POST["nombre_ped_al"]
    fecha_pedido_ped_al=request.POST["fecha_pedido_ped_al"]
    direccion_ped_al=request.POST["direccion_ped_al"]
    modo_pago_ped_al=request.POST["modo_pago_ped_al"]


    nuevoPedido=Pedido.objects.create(nombre_ped_al=nombre_ped_al, fecha_pedido_ped_al=fecha_pedido_ped_al, direccion_ped_al=direccion_ped_al,
    modo_pago_ped_al=modo_pago_ped_al, cliente=clienteSeleccionado)

    messages.success(request, 'Pedido Guardado exitosamente')
    return redirect('/pedido')

def eliminarPedido(request,codigo_ped_al):
    pedidoEliminar=Pedido.objects.get(codigo_ped_al=codigo_ped_al)
    pedidoEliminar.delete()
    messages.success(request, 'Pedido Eliminado exitosamente')
    return redirect('/pedido')

def editarPedido(request,codigo_ped_al):
    pedidoEditar=Pedido.objects.get(codigo_ped_al=codigo_ped_al)
    clientesBdd=Cliente.objects.all()
    return render(request,
    'editarPedido.html',{'pedido':pedidoEditar, 'clientes':clientesBdd})

def procesarActualizacionPedido(request):
    codigo_ped_al=request.POST["codigo_ped_al"]
    id_cli=request.POST["id_cli"]
    clienteSeleccionado=Cliente.objects.get(codigo_cli_al=id_cli)
    nombre_ped_al=request.POST["nombre_ped_al"]
    fecha_pedido_ped_al=request.POST["fecha_pedido_ped_al"]
    direccion_ped_al=request.POST["direccion_ped_al"]
    modo_pago_ped_al=request.POST["modo_pago_ped_al"]

    #Insertando datos mediante el ORM de DJANGO
    pedidoEditar=Pedido.objects.get(codigo_ped_al=codigo_ped_al)
    pedidoEditar.cliente=clienteSeleccionado
    pedidoEditar.nombre_ped_al=nombre_ped_al
    pedidoEditar.fecha_pedido_ped_al=fecha_pedido_ped_al
    pedidoEditar.direccion_ped_al=direccion_ped_al
    pedidoEditar.modo_pago_ped_al=modo_pago_ped_al

    pedidoEditar.save()
    messages.success(request,
      'Pedido ACTUALIZADO Exitosamente')
    return redirect('/pedido')

def listadoPlatillos(request):
    platillosBdd=Platillo.objects.all()
    tiposBdd=Tipo.objects.all()
    return render(request,'platillo.html',{'platillos':platillosBdd, 'tipos':tiposBdd, 'navbar': 'platillo'})

def guardarPlatillo(request):
    id_tip=request.POST["id_tip"]
    tipoSeleccionado=Tipo.objects.get(codigo_tip_al=id_tip)
    nombre_pla_al=request.POST["nombre_pla_al"]
    descripcion_pla_al=request.POST["descripcion_pla_al"]
    precio_pla_al=request.POST["precio_pla_al"]
    disponibilidad_pla_al=request.POST["disponibilidad_pla_al"]

    nuevoPlatillo=Platillo.objects.create(nombre_pla_al=nombre_pla_al, descripcion_pla_al=descripcion_pla_al, precio_pla_al=precio_pla_al,
    disponibilidad_pla_al=disponibilidad_pla_al, tipo=tipoSeleccionado)

    messages.success(request, 'Platillo Guardado exitosamente')
    return redirect('/platillo')

def eliminarPlatillo(request,codigo_pla_al):
    platilloEliminar=Platillo.objects.get(codigo_pla_al=codigo_pla_al)
    platilloEliminar.delete()
    messages.success(request, 'Platillo Eliminado exitosamente')
    return redirect('/platillo')

def editarPlatillo(request,codigo_pla_al):
    platilloEditar=Platillo.objects.get(codigo_pla_al=codigo_pla_al)
    tiposBdd=Tipo.objects.all()
    return render(request,
    'editarPlatillo.html',{'platillo':platilloEditar, 'tipos':tiposBdd})

def procesarActualizacionPlatillo(request):
    codigo_pla_al=request.POST["codigo_pla_al"]
    id_tip=request.POST["id_tip"]
    tipoSeleccionado=Tipo.objects.get(codigo_tip_al=id_tip)
    nombre_pla_al=request.POST["nombre_pla_al"]
    descripcion_pla_al=request.POST["descripcion_pla_al"]
    precio_pla_al=request.POST["precio_pla_al"]
    disponibilidad_pla_al=request.POST["disponibilidad_pla_al"]
    #Insertando datos mediante el ORM de DJANGO
    platilloEditar=Platillo.objects.get(codigo_pla_al=codigo_pla_al)
    platilloEditar.tipo=tipoSeleccionado
    platilloEditar.nombre_pla_al=nombre_pla_al
    platilloEditar.descripcion_pla_al=descripcion_pla_al
    platilloEditar.precio_pla_al=precio_pla_al
    platilloEditar.disponibilidad_pla_al=disponibilidad_pla_al

    platilloEditar.save()
    messages.success(request,
      'Platillo ACTUALIZADO Exitosamente')
    return redirect('/platillo')

def listadoDetalles(request):
    detallesBdd=Detalle.objects.all()
    pedidosBdd=Pedido.objects.all()
    platillosBdd=Platillo.objects.all()
    return render(request,'detalle.html',{'detalles':detallesBdd, 'pedidos':pedidosBdd, 'platillos':platillosBdd, 'navbar': 'detalle'})

def guardarDetalle(request):
    id_ped=request.POST["id_ped"]
    pedidoSeleccionado=Pedido.objects.get(codigo_ped_al=id_ped)
    id_pla=request.POST["id_pla"]
    platilloSeleccionado=Platillo.objects.get(codigo_pla_al=id_pla)
    fecha_detalle_det_al=request.POST["fecha_detalle_det_al"]
    cantidad_det_al=request.POST["cantidad_det_al"]
    descripcion_det_al=request.POST["descripcion_det_al"]
    total_det_al=request.POST["total_det_al"]

    nuevoDetalle=Detalle.objects.create(fecha_detalle_det_al=fecha_detalle_det_al, cantidad_det_al=cantidad_det_al, descripcion_det_al=descripcion_det_al,
    total_det_al=total_det_al, pedido=pedidoSeleccionado, platillo=platilloSeleccionado)

    messages.success(request, 'Detalle Guardado exitosamente')
    return redirect('/detalle')

def eliminarDetalle(request,codigo_det_al):
    detalleEliminar=Detalle.objects.get(codigo_det_al=codigo_det_al)
    detalleEliminar.delete()
    messages.success(request, 'Detalle Eliminado exitosamente')
    return redirect('/detalle')

def editarDetalle(request,codigo_det_al):
    detalleEditar=Detalle.objects.get(codigo_det_al=codigo_det_al)
    pedidosBdd=Pedido.objects.all()
    platillosBdd=Platillo.objects.all()
    return render(request,
    'editarDetalle.html',{'detalle':detalleEditar, 'pedidos':pedidosBdd, 'platillos':platillosBdd})

def procesarActualizacionDetalle(request):
    codigo_det_al=request.POST["codigo_det_al"]
    id_ped=request.POST["id_ped"]
    pedidoSeleccionado=Pedido.objects.get(codigo_ped_al=id_ped)
    id_pla=request.POST["id_pla"]
    platilloSeleccionado=Platillo.objects.get(codigo_pla_al=id_pla)
    fecha_detalle_det_al=request.POST["fecha_detalle_det_al"]
    cantidad_det_al=request.POST["cantidad_det_al"]
    descripcion_det_al=request.POST["descripcion_det_al"]
    total_det_al=request.POST["total_det_al"]
    #Insertando datos mediante el ORM de DJANGO
    detalleEditar=Detalle.objects.get(codigo_det_al=codigo_det_al)
    detalleEditar.pedido=pedidoSeleccionado
    detalleEditar.platillo=platilloSeleccionado
    detalleEditar.fecha_detalle_det_al=fecha_detalle_det_al
    detalleEditar.cantidad_det_al=cantidad_det_al
    detalleEditar.descripcion_det_al=descripcion_det_al
    detalleEditar.total_det_al=total_det_al


    detalleEditar.save()
    messages.success(request,
      'Detalle ACTUALIZADO Exitosamente')
    return redirect('/detalle')

def listadoRecetas(request):
    recetasBdd=Receta.objects.all()
    platillosBdd=Platillo.objects.all()
    ingredientesBdd=Ingrediente.objects.all()
    return render(request,'receta.html',{'recetas':recetasBdd, 'platillos':platillosBdd, 'ingredientes':ingredientesBdd, 'navbar': 'receta'})

def guardarReceta(request):
    id_pla=request.POST["id_pla"]
    platilloSeleccionado=Platillo.objects.get(codigo_pla_al=id_pla)
    id_ing=request.POST["id_ing"]
    ingredienteSeleccionado=Ingrediente.objects.get(codigo_ing_al=id_ing)
    nombre_rec_al=request.POST["nombre_rec_al"]
    descripcion_rec_al=request.POST["descripcion_rec_al"]
    tipo_preparacion_rec_al=request.POST["tipo_preparacion_rec_al"]
    pasos_rec_al=request.POST["pasos_rec_al"]


    nuevoReceta=Receta.objects.create(nombre_rec_al=nombre_rec_al, descripcion_rec_al=descripcion_rec_al, tipo_preparacion_rec_al=tipo_preparacion_rec_al,
    pasos_rec_al=pasos_rec_al, platillo=platilloSeleccionado, ingrediente=ingredienteSeleccionado)

    messages.success(request, 'Receta Guardado exitosamente')
    return redirect('/receta')

def eliminarReceta(request,codigo_rec_al):
    recetaEliminar=Receta.objects.get(codigo_rec_al=codigo_rec_al)
    recetaEliminar.delete()
    messages.success(request, 'Receta Eliminado exitosamente')
    return redirect('/receta')

def editarReceta(request,codigo_rec_al):
    recetaEditar=Receta.objects.get(codigo_rec_al=codigo_rec_al)
    platillosBdd=Platillo.objects.all()
    ingredientesBdd=Ingrediente.objects.all()
    return render(request,
    'editarReceta.html',{'receta':recetaEditar, 'platillos':platillosBdd, 'ingredientes':ingredientesBdd})

def procesarActualizacionReceta(request):
    codigo_rec_al=request.POST["codigo_rec_al"]
    id_pla=request.POST["id_pla"]
    platilloSeleccionado=Platillo.objects.get(codigo_pla_al=id_pla)
    id_ing=request.POST["id_ing"]
    ingredienteSeleccionado=Ingrediente.objects.get(codigo_ing_al=id_ing)
    nombre_rec_al=request.POST["nombre_rec_al"]
    descripcion_rec_al=request.POST["descripcion_rec_al"]
    tipo_preparacion_rec_al=request.POST["tipo_preparacion_rec_al"]
    pasos_rec_al=request.POST["pasos_rec_al"]

    #Insertando datos mediante el ORM de DJANGO
    recetaEditar=Receta.objects.get(codigo_rec_al=codigo_rec_al)
    recetaEditar.platillo=platilloSeleccionado
    recetaEditar.ingrediente=ingredienteSeleccionado
    recetaEditar.nombre_rec_al=nombre_rec_al
    recetaEditar.descripcion_rec_al=descripcion_rec_al
    recetaEditar.tipo_preparacion_rec_al=tipo_preparacion_rec_al
    recetaEditar.pasos_rec_al=pasos_rec_al


    recetaEditar.save()
    messages.success(request,
      'Receta ACTUALIZADO Exitosamente')
    return redirect('/receta')
