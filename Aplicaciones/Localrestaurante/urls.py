from django.urls import path
from . import views

urlpatterns=[
    path('', views.listadoProvincias, name='provincia'),
    path('guardarProvincia/', views.guardarProvincia),
    path('eliminarProvincia/<codigo_pro_al>', views.eliminarProvincia),
    path('editarProvincia/<codigo_pro_al>', views.editarProvincia),
    path('procesarActualizacionProvincia/', views.procesarActualizacionProvincia),

    path('tipo/', views.listadoTipos, name='tipo'),
    path('guardarTipo/', views.guardarTipo),
    path('eliminarTipo/<codigo_tip_al>', views.eliminarTipo),
    path('editarTipo/<codigo_tip_al>', views.editarTipo),
    path('procesarActualizacionTipo/', views.procesarActualizacionTipo),

    path('ingrediente/', views.listadoIngredientes, name='ingrediente'),
    path('guardarIngrediente/', views.guardarIngrediente),
    path('eliminarIngrediente/<codigo_ing_al>', views.eliminarIngrediente),
    path('editarIngrediente/<codigo_ing_al>', views.editarIngrediente),
    path('procesarActualizacionIngrediente/', views.procesarActualizacionIngrediente),

    path('cliente/', views.listadoClientes, name='cliente'),
    path('guardarCliente/', views.guardarCliente),
    path('eliminarCliente/<codigo_cli_al>', views.eliminarCliente),
    path('editarCliente/<codigo_cli_al>', views.editarCliente),
    path('procesarActualizacionCliente/', views.procesarActualizacionCliente),

    path('pedido/', views.listadoPedidos, name='pedido'),
    path('guardarPedido/', views.guardarPedido),
    path('eliminarPedido/<codigo_ped_al>', views.eliminarPedido),
    path('editarPedido/<codigo_ped_al>', views.editarPedido),
    path('procesarActualizacionPedido/', views.procesarActualizacionPedido),

    path('platillo/', views.listadoPlatillos, name='platillo'),
    path('guardarPlatillo/', views.guardarPlatillo),
    path('eliminarPlatillo/<codigo_pla_al>', views.eliminarPlatillo),
    path('editarPlatillo/<codigo_pla_al>', views.editarPlatillo),
    path('procesarActualizacionPlatillo/', views.procesarActualizacionPlatillo),

    path('detalle/', views.listadoDetalles, name='detalle'),
    path('guardarDetalle/', views.guardarDetalle),
    path('eliminarDetalle/<codigo_det_al>', views.eliminarDetalle),
    path('editarDetalle/<codigo_det_al>', views.editarDetalle),
    path('procesarActualizacionDetalle/', views.procesarActualizacionDetalle),

    path('receta/', views.listadoRecetas, name='receta'),
    path('guardarReceta/', views.guardarReceta),
    path('eliminarReceta/<codigo_rec_al>', views.eliminarReceta),
    path('editarReceta/<codigo_rec_al>', views.editarReceta),
    path('procesarActualizacionReceta/', views.procesarActualizacionReceta),
]
