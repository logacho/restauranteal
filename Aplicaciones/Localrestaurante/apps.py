from django.apps import AppConfig


class LocalrestauranteConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'Aplicaciones.Localrestaurante'
